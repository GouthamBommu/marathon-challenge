const { Model } = require("sequelize");

export default class Event extends Model {
  static init(sequelize, DataTypes) {
    return super.init(
      {
        name: {
          type: DataTypes.STRING,
          unique: true,
          allowNull: false,
        },
        price: {
          type: DataTypes.DECIMAL(5, 2),
          allowNull: false,
        },
      },
      { sequelize, modelName: "event", timestamps: true }
    );
  }
}
