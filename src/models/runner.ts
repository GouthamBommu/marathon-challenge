const { Model } = require("sequelize");
const bcrypt = require("bcryptjs");
const VISA_PROPERTIES = {
  prefixLength: 1,
  prefix: [4],
  length: 16,
};
const MASTER_PROPETIES = {
  prefixLength: 2,
  prefix: [51, 52, 53, 54, 55],
  length: 16,
};
const CARD_TYPES = {
  VISA: { properties: VISA_PROPERTIES },
  MASTER: { properties: MASTER_PROPETIES },
};

export default class Runner extends Model {
  static isValidCard(cardType, cardNumber) {
    let isValidPrefix = false;
    let isValidLength = false;
    const properties = CARD_TYPES[cardType]["properties"];
    isValidPrefix =
      properties["prefix"].find(
        (val) => val == cardNumber.slice(0, properties["prefixLength"])
      ) !== undefined
        ? true
        : false;
    isValidLength = properties["length"] == cardNumber.length ? true : false;

    if (!(isValidLength && isValidPrefix)) {
      throw Error(`Invalid ${cardType} type`);
    }
    return true;
  }

  static init(sequelize, DataTypes) {
    return super.init(
      {
        name: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        email: {
          type: DataTypes.STRING,
          unique: true,
          allowNull: false,
          validate: {
            isEmail: true,
            notNull: {
              msg: "Please enter your email",
            },
          },
        },
        nameOnCard: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        cardNumber: {
          type: DataTypes.STRING,
          allowNull: false,
          validate: {
            isInt(value) {
              if (parseInt(value) === NaN) {
                throw Error("Invalid card number");
              }
            },
          },
        },
        cardType: {
          type: DataTypes.ENUM,
          values: ["VISA", "MASTER"],
        },
      },
      {
        sequelize,
        modelName: "runner",
        timestamps: true,
        validate: {
          cardValidate() {
            Runner.isValidCard(this.cardType, this.cardNumber);
          },
        },
      }
    );
  }
}
