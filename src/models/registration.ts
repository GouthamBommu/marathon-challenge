const { Model } = require("sequelize");

export default class Registration extends Model {
  static init(sequelize, DataTypes) {
    return super.init(
      {
        runnerId: {
          type: DataTypes.INTEGER,
          allowNull: false,
        },
        eventId: {
          type: DataTypes.INTEGER,
          allowNull: false,
        },
        date: {
          type: DataTypes.DATEONLY,
        },
      },
      { sequelize, modelName: "registration", timestamps: true }
    );
  }

  static associate(models) {
    models.Event.belongsToMany(models.Runner, { through: "registrations" });
    models.Runner.belongsToMany(models.Event, { through: "registrations" });
  }
}
