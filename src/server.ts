import GraphQL from "./utils/graphql";
import {
  typeDefs as eventDefs,
  resolvers as eventResolvers,
} from "./modules/event/event.schema";
import {
  typeDefs as runnerDefs,
  resolvers as runnerResolvers,
} from "./modules/runner/runner.schema";
import {
  typeDefs as registrationDefs,
  resolvers as registrationResolvers,
} from "./modules/registration/registration.schema";

export default class Main {
  private graphql = new GraphQL();
  constructor() {
    // GraphQL Server
    this.graphql.addToSchema(eventDefs, eventResolvers);
    this.graphql.addToSchema(runnerDefs, runnerResolvers);
    this.graphql.addToSchema(registrationDefs, registrationResolvers);

    this.graphql.start();
  }
}
const main = new Main();
