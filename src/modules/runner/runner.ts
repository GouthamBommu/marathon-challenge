import Database from "@utils/database";
const runner = Database.Runner;

export default class JobGroup {
  public async get(id = undefined) {
    const condition = id
      ? {
          where: {
            id: id,
          },
        }
      : {};

    return id ? await runner.findOne(condition) : await runner.findAll();
  }

  public async create(input, options = {}) {
    const group = await runner.create(input, options);
    return group;
  }

  public async update(id, input) {
    const response = await runner.update(input, { where: { id: id } });
    return response[0];
  }

  public async delete(id) {
    const response = await runner.destroy({ where: { id: id } });
    return response;
  }
}
