import gql from "graphql-tag";
import { GraphQLResolverMap } from "@apollographql/apollo-tools";
import Runner from "./runner";
const runner = new Runner();

export const typeDefs = gql`
  input RunnerInput {
    name: String
    email: String
    nameOnCard: String
    cardNumber: String
    cardType: String
  }

  type Runner {
    id: ID
    name: String
    email: String
    nameOnCard: String
    cardType: String
  }

  extend type Query {
    getRunners: [Runner]
  }

  extend type Mutation {
    createRunner(input: RunnerInput!): Runner
    updateRunner(id: ID!, input: RunnerInput!): Boolean
    deleteRunner(id: ID!): Boolean
  }
`;

export const resolvers: GraphQLResolverMap<any> = {
  Query: {
    getRunners: async () => await runner.get(),
  },

  Mutation: {
    createRunner: async (_, { input }) => await runner.create(input),
    updateRunner: async (_, { id, input }) => await runner.update(id, input),
    deleteRunner: async (_, { id }) => await runner.delete(id),
  },
};
