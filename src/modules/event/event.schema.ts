import gql from "graphql-tag";
import { GraphQLResolverMap } from "@apollographql/apollo-tools";
import Event from "./event";
const event = new Event();

export const typeDefs = gql`
  input EventInput {
    name: String
    price: String
  }

  type Event {
    id: ID
    name: String
    price: String
  }

  extend type Query {
    getEvents: [Event]
  }

  extend type Mutation {
    createEvent(input: EventInput!): Event
    updateEvent(id: ID!, input: EventInput!): Boolean
    deleteEvent(id: ID!): Boolean
  }
`;

export const resolvers: GraphQLResolverMap<any> = {
  Query: {
    getEvents: async () => await event.get(),
  },

  Mutation: {
    createEvent: async (_, { input }) => await event.create(input),
    updateEvent: async (_, { id, input }) => await event.update(id, input),
    deleteEvent: async (_, { id }) => await event.delete(id),
  },
};
