import Database from "@utils/database";
const event = Database.Event;

export default class JobGroup {
  public async get(id = undefined) {
    const condition = id
      ? {
          where: {
            id: id,
          },
        }
      : {};

    return id ? await event.findOne(condition) : await event.findAll();
  }

  public async create(input, options = {}) {
    const group = await event.create(input, options);
    return group;
  }

  public async update(id, input) {
    const response = await event.update(input, { where: { id: id } });
    return response[0];
  }

  public async delete(id) {
    const response = await event.destroy({ where: { id: id } });
    return response;
  }
}
