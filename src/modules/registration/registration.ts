import Database from "@utils/database";
const seq = Database.sequelize;
const { Op } = require("sequelize");
const registration = Database.Registration;
import Event from "../event/event";
const event = new Event();
import Runner from "../runner/runner";
const runner = new Runner();

const moment = require("moment");

export default class Registration {
  public async getSummary(eventId, month, year) {
    let eventInfo = await event.get(eventId);
    let eventPrice = eventInfo["price"];

    let dailyResult = await registration.findAll({
      attributes: [
        "date",
        [seq.fn("count", seq.col("runnerId")), "numOfRunners"],
      ],
      where: {
        [Op.and]: [
          seq.where(seq.fn("year", seq.col("date")), year),
          seq.where(seq.fn("month", seq.col("date")), month),
          { eventId: eventId },
        ],
      },
      group: "date",
    });

    dailyResult = Array.isArray(dailyResult) ? dailyResult : [dailyResult];

    let dailyCollection = dailyResult.map((each) => {
      let data = each.dataValues;
      let output = {
        date: data.date,
        total: data.numOfRunners * eventPrice,
      };
      return output;
    });

    let totalPrice = dailyCollection.reduce((total, day) => {
      return (total += day.total);
    }, 0);

    let summary = {
      monthCollection: {
        month: month,
        year: year,
        total: totalPrice,
        daily: dailyCollection,
      },
    };

    return summary;
  }

  public async create(eventId, runnerInput) {
    const result = await seq.transaction(async (t) => {
      /* CREATE RUNNER*/
      let newRunner = await runner.create(runnerInput, { transaction: t });

      /*Todo Payment*/

      /* LINK TO EVENT*/
      let input = {
        eventId: eventId,
        runnerId: newRunner.id,
        date: moment().format("YYYY-MM-DD"),
      };
      await registration.create(input, { transaction: t });

      return true;
    });

    return result;
  }
}
