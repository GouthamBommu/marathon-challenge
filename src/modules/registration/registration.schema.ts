import gql from "graphql-tag";
import { GraphQLResolverMap } from "@apollographql/apollo-tools";
import Registration from "./registration";
const registration = new Registration();

export const typeDefs = gql`
  extend type Event {
    summary: Summary
  }

  type Summary {
    monthCollection: MonthCollection
  }

  type MonthCollection {
    month: String
    year: String
    total: String
    daily: [DailyCollection]
  }

  type DailyCollection {
    date: String
    total: String
  }

  extend type Query {
    getSummary(eventId: ID!, month: ID!, year: ID!): Summary
  }

  extend type Mutation {
    createRegistration(eventId: ID!, runner: RunnerInput!): Boolean
  }
`;

export const resolvers: GraphQLResolverMap<any> = {
  Query: {
    getSummary: async (_, { eventId, month, year }) =>
      await registration.getSummary(eventId, month, year),
  },

  Mutation: {
    createRegistration: async (_, { eventId, runner }) =>
      await registration.create(eventId, runner),
  },
};
