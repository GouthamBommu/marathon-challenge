const { Sequelize } = require("sequelize");
import Event from "../models/event";
import Runner from "../models/runner";
import Registration from "../models/registration";

const sequelize = new Sequelize(
  process.env.MYSQL_DATABASE,
  process.env.MYSQL_USER,
  process.env.MYSQL_PASSWORD,
  {
    host: process.env.MYSQL_HOST,
    dialect: "mysql",
  }
);

const models = {
  Event: Event.init(sequelize, Sequelize),
  Runner: Runner.init(sequelize, Sequelize),
  Registration: Registration.init(sequelize, Sequelize),
};

// Run `.associate` if it exists,
// ie create relationships in the ORM
Object.values(models)
  .filter((model) => typeof model.associate === "function")
  .forEach((model) => model.associate(models));

const db = {
  ...models,
  sequelize,
};
export default db;
